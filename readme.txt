This archive contains Python code and supporting files for the tasks of a Coursera MOOC. 

Course Title: Python Data Structures

Instructor: Charles Severance

Provider: University of Michigan

Summary: 
This course will introduce the core data structures of the Python programming language. 
We will move past the basics of procedural programming and explore how we can use the 
Python built-in data structures such as lists, dictionaries, and tuples to perform increasingly 
complex data analysis. This course will cover Chapters 6-10 of the textbook “Python for Informatics”.  

Link: https://www.coursera.org/learn/python-data/

Session Date: February 8th - April 4th 2016

import re
fname = raw_input("Enter file name: ")
if len(fname) < 1 : fname = "mbox-short.txt"
fh = open(fname)

counts = dict()
for line in fh:
    if not line.startswith("From "): continue
    #words = line.split()
    #name = words[1].strip()
    name = re.findall("\S+\s", line)[1].strip()
    counts[name] = counts.get(name, 0) + 1

maximum = 0
nameMax = ""
for name in counts:
    if counts[name] > maximum:
        maximum = counts[name]
        nameMax = name
print nameMax, maximum

#import re
fname = raw_input("Enter file name: ")
if len(fname) < 1 : fname = "mbox-short.txt"

fh = open(fname)
count = 0
for line in fh:
    if not line.startswith("From "): continue
    count += 1
    lst = line.split()
    print lst[1]
    # addresses = re.findall("\S+@+\S", line)
    # if len(addresses) > 0:
    #    print addresses[0]
print "There were", count, "lines in the file with From as the first word"

import re
fname = raw_input("Enter file name: ")
if len(fname) < 1 : fname = "mbox-short.txt"
fh = open(fname)

counts = dict()
for line in fh:
    if not line.startswith("From "): continue
    #words = line.split()
    #name = words[1].strip()
    #print line
    time = re.findall("([0-9]+):", line)[0].strip()
    #print time
    counts[time] = counts.get(time, 0) + 1

lst = sorted([(time, count) for time,count in counts.items()])
for tup in lst:
    print tup[0], tup[1]

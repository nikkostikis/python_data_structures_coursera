import re
# Use the file name mbox-short.txt as the file name
fname = raw_input("Enter file name: ")
fh = open(fname)
s = 0
count = 0
for line in fh:
    if not line.startswith("X-DSPAM-Confidence:") : continue
    print line
    # there could be multiple numbers in a line(?)
    for y in [ float(x) for x in re.findall( '[0-9]+[.][0-9]*', line ) ]:
        print y
        s = s + y
        count += 1
mean = s / count
print "Average spam confidence: ", mean
print "Done"
